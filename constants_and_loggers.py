"""Definition of constants and loggers."""

import logging
import sys

from PyQt5 import QtGui


# CONSTANTS
COLOR = QtGui.QColor(0, 255, 0)
DEFAULT_FONT_SIZE = 16
MIN_FONT_SIZE = 5
MAX_FONT_SIZE = 99
if sys.platform == 'linux':
    FONT = QtGui.QFont('Noto Sans Mono', DEFAULT_FONT_SIZE)
else:
    FONT = QtGui.QFont('Consolas', DEFAULT_FONT_SIZE)
DEFAULT_LANG = 'en'
TXT_UNDO_LEN = 50


# LOGGERS
def get_logger(name: str, add_handler=False):
    """Creates logger with name name."""
    new_logger = logging.getLogger(name)
    if add_handler:
        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            '%(levelname)s - %(name)s - %(filename)s:%(lineno)s >>> %(message)s')
        handler.setFormatter(formatter)
        new_logger.addHandler(handler)
    return new_logger


logger = get_logger('base', add_handler=True)
logger_app_highlight = get_logger('base.app_highlight')
logger_txt_highlight = get_logger('base.txt_highlight')
logger_txt_history = get_logger('base.txt_history')

# LOGGING LEVELS
logger.setLevel(logging.WARNING)
# logger_app_highlight.setLevel(logging.DEBUG)
# logger_txt_highlight.setLevel(logging.DEBUG)
# logger_txt_history.setLevel(logging.DEBUG)
