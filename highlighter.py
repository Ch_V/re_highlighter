"""Definition of RE_highlighter application."""

from contextlib import contextmanager
import ctypes
import json
import importlib
import os
import re
import sys

from PyQt5 import QtCore, QtGui, QtWidgets

from constants_and_loggers import (COLOR, DEFAULT_LANG,
                                   DEFAULT_FONT_SIZE, MIN_FONT_SIZE, MAX_FONT_SIZE, FONT,
                                   logger, logger_app_highlight)
from widgets import TextEdit, ColorDialog


# Show icon in Windows taskbar.
if sys.platform == 'win32':
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
        'mycompany.myproduct.subproduct.version')


class App(QtWidgets.QApplication):
    """Application itself."""
    __version__ = "1.5"

    def __init__(self, *args):
        super().__init__(*args)
        self.application_start()

    def application_start(self):
        """Checks if `langs` languages directory is in place and saves available languages.
        Then creates and shows main application window.
        """
        if not os.path.isdir('langs'):
            os.mkdir('langs')
            logger.warning(
                '`langs` languages directory was not in place!!! '
                'Empty `langs` languages directory is created.'
            )
        regex = re.compile(r'(\w\w)\.py', re.I)
        self.langs_available = \
            [m.group(1).title() for m in [regex.fullmatch(filename)
                                          for filename in os.listdir('langs')] if m]
        global DEFAULT_LANG
        if DEFAULT_LANG.title() not in self.langs_available:
            if len(self.langs_available) == 0:
                logger.warning(
                    'No language file was found in `langs` languages directory!!! '
                    'No inscription would be displayed!'
                )
            else:
                logger.info(f'`{DEFAULT_LANG}` was not found in available languages. '
                            f'Trying to use `{self.langs_available[0].lower()}` as default language.')
                DEFAULT_LANG = self.langs_available[0].lower()

        self.window = Window()
        self.setWindowIcon(QtGui.QIcon('icon.ico'))
        self.window.show()

    @QtCore.pyqtSlot(str)
    def lang_change(self, value):
        """Changes language with main application window restart."""
        self.window.preferences['lang'] = value.lower()
        with self.save_texts():
            self.window.close()
            self.window.deleteLater()
            self.window = Window()
        self.window.show()

    @contextmanager
    def save_texts(self):
        """Saves and restores texts from pattern, flags and text widgets.
        And group and match_n too.
        """
        patter = self.window.pattern_line.text()
        flags = self.window.flags_line.text()
        txt = self.window.txt.toPlainText()
        group = self.window.groups_cbbx.currentText()
        match_n = self.window.match_n_cbbx.currentText()
        yield
        self.window.pattern_line.setText(patter)
        self.window.flags_line.setText(flags)
        self.window.txt.setPlainText(txt)
        self.window.groups_cbbx.setCurrentText(group)
        self.window.match_n_cbbx.setCurrentText(match_n)


class Window(QtWidgets.QMainWindow):
    """Main application window."""
    def __init__(self):
        super().__init__(windowTitle='RE highlighter')
        # Load preferences.
        self.load_preferences()

        # Set central widget.
        self.cw = QtWidgets.QWidget(self)
        self.setCentralWidget(self.cw)

        # Pattern input line.
        self.pattern_line = QtWidgets.QLineEdit()
        self.pattern_line.setPlaceholderText(lang.get('pattern_line_placeholder', ''))
        self.pattern_line.setFont(FONT)

        # Flags input line.
        self.flags_line = QtWidgets.QLineEdit()
        self.flags_line.setPlaceholderText(lang.get('flags_line_placeholder', ''))
        self.flags_line.setFont(FONT)
        self.flags_line.setToolTip(lang.get('flags_line_tooltip', ''))
        # Check if flags are correct and safe (they would be fed to eval!)
        regex = QtCore.QRegExp(
            r"(?:re\.[IMSXAUL])*(?: ?\| ?re\.[IMSXAUL])*"
        )
        self.flags_line.setValidator(QtGui.QRegExpValidator(regex))

        # Text field for highlighted text.
        self.txt = TextEdit(lang=lang)
        self.txt.setPlaceholderText(lang.get('txt_placeholder', ''))
        self.txt.setFont(FONT)
        self.txt.setLineWrapMode(QtWidgets.QTextEdit.NoWrap)

        # Bind Alt + * shortcuts to pattern, flags and text.
        pattern_line_lb = QtWidgets.QLabel('&1', self)
        flags_line_lb = QtWidgets.QLabel('&2', self)
        txt_lb = QtWidgets.QLabel('&3', self)
        big_offset = int(1e9)
        pattern_line_lb.move(big_offset, big_offset)
        flags_line_lb.move(big_offset, big_offset)
        txt_lb.move(big_offset, big_offset)
        pattern_line_lb.setBuddy(self.pattern_line)
        flags_line_lb.setBuddy(self.flags_line)
        txt_lb.setBuddy(self.txt)

        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        splitter.addWidget(self.pattern_line)
        splitter.addWidget(self.flags_line)
        splitter.setStretchFactor(0, 4)
        splitter.setStretchFactor(1, 1)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(splitter)
        vbox.addWidget(self.txt)
        self.cw.setLayout(vbox)

        ################################ TOOLBAR ################################
        BUTTON_FONT = QtGui.QFont('Calibri', 16, 50)
        TOOLBAR_MARGIN = 3

        self.toolbar = self.addToolBar('Toolbar')
        self.toolbar.setMovable(False)
        btn_size_policy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding
        )

        self.groups_lbl = QtWidgets.QLabel(lang.get('groups_lbl', ''), self)
        self.groups_lbl.setFont(BUTTON_FONT)
        self.groups_lbl.setSizePolicy(btn_size_policy)
        self.groups_lbl.setStyleSheet(f'margin-left: {TOOLBAR_MARGIN};')
        self.groups_model = QtCore.QStringListModel(['0'])
        self.groups_cbbx = QtWidgets.QComboBox(self)
        self.groups_cbbx.setModel(self.groups_model)
        self.groups_cbbx.setFont(BUTTON_FONT)
        self.groups_cbbx.setSizePolicy(btn_size_policy)
        self.groups_cbbx.setSizeAdjustPolicy(self.groups_cbbx.AdjustToContents)
        _GROUPS_TOOLTIP = lang.get('groups_tooltip', '')
        self.groups_lbl.setToolTip(_GROUPS_TOOLTIP)
        self.groups_cbbx.setToolTip(_GROUPS_TOOLTIP)
        self.groups_cbbx.currentTextChanged.connect(self.highlight)
        self.groups_cbbx.setStyleSheet(f'margin-right:{TOOLBAR_MARGIN}; padding-left:{TOOLBAR_MARGIN};')
        self.groups_lbl.setBuddy(self.groups_cbbx)

        self.wrap_btn = QtWidgets.QToolButton(self, icon=QtGui.QIcon('wrap_icon.svg'),
                                              text=lang.get('wrap_btn_btn', ''))
        self.wrap_btn.setFont(BUTTON_FONT)
        self.wrap_btn.setSizePolicy(btn_size_policy)
        self.wrap_btn.setMinimumWidth(50)
        self.wrap_btn.setToolTip(lang.get('wrap_btn_tooltip', ''))
        self.wrap_btn.setCheckable(True)
        self.wrap_btn.toggled.connect(self.wrap_switch)
        self.wrap_btn.setStyleSheet(f'margin-right:{TOOLBAR_MARGIN}; padding-left:{TOOLBAR_MARGIN};')
        if self._wrap:
            self.wrap_btn.toggle()

        self.color_change_btn = QtWidgets.QToolButton(self, text=lang.get('color_change_btn', ''))
        self.set_color_rectangle()
        # self.color_change_btn.setToolButtonStyle(QtCore.Qt.ToolButtonStyle.ToolButtonTextBesideIcon)
        self.color_change_btn.setToolTip(lang.get('color_change_btn_tooltip', ''))
        self.color_change_btn.setFont(BUTTON_FONT)
        self.color_change_btn.setSizePolicy(btn_size_policy)
        self.color_change_btn.setMinimumWidth(50)
        self.color_change_btn.clicked.connect(self.color_change_slot)
        self.color_change_btn.setStyleSheet(f'margin-left: {TOOLBAR_MARGIN}; margin-right:{TOOLBAR_MARGIN};')
        self.color_dialog = ColorDialog(lang=lang)

        self.font_size_lbl = QtWidgets.QLabel(lang.get('font_size_lbl', ''), self)
        self.font_size_lbl.setFont(BUTTON_FONT)
        self.font_size_lbl.setSizePolicy(btn_size_policy)
        self.font_size_lbl.setStyleSheet(f'margin-left: {TOOLBAR_MARGIN};')
        self.font_size_model = QtCore.QStringListModel((str(n) for n in range(5, 100)))
        self.font_size_cbbx = QtWidgets.QComboBox(self)
        self.font_size_cbbx.setModel(self.font_size_model)
        self.font_size_cbbx.setFont(BUTTON_FONT)
        FONTSIZE_TOOLTIP = lang.get('font_size_tooltip', '')
        self.font_size_lbl.setToolTip(FONTSIZE_TOOLTIP)
        self.font_size_cbbx.setToolTip(FONTSIZE_TOOLTIP)
        self.font_size_cbbx.setMinimumWidth(55)
        self.font_size_cbbx.setSizePolicy(btn_size_policy)
        self.font_size_cbbx.currentIndexChanged[str].connect(self.set_font_size)
        self.font_size_cbbx.setStyleSheet(f'margin-right:{TOOLBAR_MARGIN};padding-left:{TOOLBAR_MARGIN};')
        self.font_size_cbbx.setMaxVisibleItems(15)
        self.font_size_cbbx.setCurrentText(str(FONT.pointSize()))
        self.font_size_lbl.setBuddy(self.font_size_cbbx)

        self._font_size_reset = QtWidgets.QShortcut('Ctrl+0', self)
        self._font_size_reset.activated.connect(self.font_size_reset)
        self._font_size_increase = QtWidgets.QShortcut('Ctrl+=', self)
        self._font_size_increase.activated.connect(self.font_size_increase)
        self._font_size_increase = QtWidgets.QShortcut('Ctrl++', self)
        self._font_size_increase.activated.connect(self.font_size_increase)
        self._font_size_decrease = QtWidgets.QShortcut('Ctrl+-', self)
        self._font_size_decrease.activated.connect(self.font_size_decrease)

        self.match_n_lbl = QtWidgets.QLabel(lang.get('match_n_lbl', ''), self)
        self.match_n_lbl.setFont(BUTTON_FONT)
        self.match_n_lbl.setSizePolicy(btn_size_policy)
        self.match_n_lbl.setStyleSheet(f'margin-left: {TOOLBAR_MARGIN};')
        self.match_n_model = QtCore.QStringListModel([lang.get('all')])
        self.match_n_cbbx = QtWidgets.QComboBox(self)
        self.match_n_cbbx.setModel(self.match_n_model)
        self.match_n_cbbx.setFont(BUTTON_FONT)
        FONTSIZE_TOOLTIP = lang.get('match_n_tooltip', '')
        self.match_n_lbl.setToolTip(FONTSIZE_TOOLTIP)
        self.match_n_cbbx.setToolTip(FONTSIZE_TOOLTIP)
        self.match_n_cbbx.setMinimumWidth(55)
        self.match_n_cbbx.setSizePolicy(btn_size_policy)
        self.match_n_cbbx.currentTextChanged.connect(self.highlight)
        self.match_n_cbbx.setStyleSheet(f'margin-right:{TOOLBAR_MARGIN};padding-left:{TOOLBAR_MARGIN};')
        self.match_n_cbbx.setMaxVisibleItems(15)
        self.match_n_cbbx.setCurrentText(str(FONT.pointSize()))
        self.match_n_lbl.setBuddy(self.match_n_cbbx)

        self._toolstretch = QtWidgets.QLabel(self)
        self._toolstretch_sp = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        self._toolstretch_sp.setHorizontalStretch(2)
        self._toolstretch.setSizePolicy(self._toolstretch_sp)

        self.lang_model = QtCore.QStringListModel(
            QtWidgets.QApplication.instance().langs_available)
        self.lang_cbbx = QtWidgets.QComboBox(self)
        self.lang_cbbx.setModel(self.lang_model)
        self.lang_cbbx.setCurrentText(self.preferences.get('lang', '').title())
        self.lang_cbbx.setFont(BUTTON_FONT)
        self.lang_cbbx.setSizePolicy(btn_size_policy)
        self.lang_cbbx.setToolTip(lang.get('lang_cbbx_tooltip', ''))
        self.lang_cbbx.setStyleSheet(f'margin-right:{TOOLBAR_MARGIN}; padding-left:{TOOLBAR_MARGIN};')
        self.lang_cbbx.currentTextChanged.connect(QtWidgets.QApplication.instance().lang_change)

        self.f11_btn = QtWidgets.QToolButton(self)
        self.f11_btn.setMinimumWidth(50)
        self.f11_btn.setIcon(QtGui.QIcon('F11_icon.svg'))
        self.f11_btn.setStyleSheet(f'margin-left: {TOOLBAR_MARGIN}; margin-right:{TOOLBAR_MARGIN};')
        self.f11_btn.setToolTip(lang.get('f11_btn_tooltip', ''))
        self.f11_btn.setSizePolicy(btn_size_policy)
        self.f11_btn.clicked.connect(self.toggle_F11)

        # Place created widgets on toolbar.
        self.toolbar.addWidget(self.wrap_btn)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.match_n_lbl)
        self.toolbar.addWidget(self.match_n_cbbx)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.groups_lbl)
        self.toolbar.addWidget(self.groups_cbbx)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.color_change_btn)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.font_size_lbl)
        self.toolbar.addWidget(self.font_size_cbbx)
        self.toolbar.addWidget(self._toolstretch)
        self.toolbar.addWidget(self.lang_cbbx)
        self.toolbar.addWidget(self.f11_btn)

        # Connections.
        self.pattern_line.textChanged.connect(self.get_regex)
        self.flags_line.textChanged.connect(self.get_regex)
        self.txt.textChanged.connect(self.highlight)

        self.regex = re.compile('')

    def load_preferences(self):
        """Tries to load `self.preferences` from `preferences.json`."""
        try:
            with open('preferences.json', 'r') as file:
                self.preferences = json.load(file)
                logger.debug('`preferences.json` is found.')
        except FileNotFoundError:
            self.preferences = {}
            logger.debug('`preferences.json` is not found')
        except json.decoder.JSONDecodeError:
            self.preferences = {}
            logger.warning('`preferences.json` is found, but could not be correctly loaded!!!')

        # highlight_color
        if 'highlight_color' in self.preferences:
            try:
                self.COLOR = QtGui.QColor(*self.preferences['highlight_color'])
            except Exception as e:
                self.COLOR = QtGui.QColor(COLOR.red(), COLOR.green(), COLOR.blue())
                logger.warning(f'highlight_color is found but not restored!!! {e}')
            else:
                logger.debug('highlight_color is restored.')
        else:
            self.COLOR = QtGui.QColor(COLOR.red(), COLOR.green(), COLOR.blue())
            logger.debug('highlight_color is not found')

        # geometry
        if 'geometry' in self.preferences:
            try:
                self.setGeometry(*self.preferences['geometry'])
            except Exception as e:
                self.resize(1000, 600)
                logger.warning(f'Geometry is found but not restored!!! {e}')
            else:
                logger.debug('Geometry is restored.')
        else:
            self.resize(1000, 600)
            logger.debug('Geometry is not found.')

        # window_state
        if 'window_state' in self.preferences:
            try:
                self.setWindowState({0: QtCore.Qt.WindowNoState,
                                     2: QtCore.Qt.WindowMaximized,
                                     4: QtCore.Qt.WindowFullScreen}[self.preferences['window_state']])
            except Exception as e:
                logger.warning(f'window_state is found but not restored!!! {e}')
            else:
                logger.debug('window_state is restored.')
        else:
            logger.debug('window_state is not found.')

        # font_size
        if 'font_size' in self.preferences:
            try:
                FONT.setPointSize(self.preferences['font_size'])
            except Exception as e:
                logger.warning(f'font_size is found but not restored!!! {e}')
            else:
                logger.debug('font_size is restored.')
        else:
            logger.debug('font_size is not found.')

        # wrap
        if 'wrap' in self.preferences:
            try:
                self._wrap = self.preferences['wrap']
            except Exception as e:
                self._wrap = False
                logger.warning(f'wrap is found but not restored!!! {e}')
            else:
                logger.debug('wrap is restored.')
        else:
            self._wrap = False
            logger.debug('wrap is not found.')

        # lang
        global lang
        if len(QtWidgets.QApplication.instance().langs_available) == 0:
            lang = {}
        else:
            if 'lang' in self.preferences:
                try:
                    lang = importlib.import_module(name=f'langs.{self.preferences["lang"]}').lang

                except Exception as e:
                    lang = importlib.import_module(name=f'langs.{DEFAULT_LANG}').lang
                    self.preferences['lang'] = DEFAULT_LANG.lower()
                    logger.warning(f'lang is found but not restored!!! {e}')
                else:
                    logger.debug('lang is restored.')
            else:
                lang = importlib.import_module(name=f'langs.{DEFAULT_LANG}').lang
                self.preferences['lang'] = DEFAULT_LANG.lower()
                logger.debug('lang is not found.')

    def keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        """Add fullscreen toggle on `F11`."""
        if event.key() == QtCore.Qt.Key_F11:
            self.toggle_F11()
        if event.key() == QtCore.Qt.Key_Escape and self.isFullScreen():
            self.toggle_F11()

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        """Add fontsize change on `Ctrl+MouseWheel`."""
        if event.modifiers() & QtCore.Qt.ControlModifier:
            current_font = self.txt.font()
            size = current_font.pointSize()
            increment = int(event.angleDelta().y()) / abs(int(event.angleDelta().y()))
            if size <= MIN_FONT_SIZE:
                increment = max(0, increment)
            if size >= MAX_FONT_SIZE:
                increment = min(0, increment)
            size += increment
            self.set_font_size(size)

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        """Saves `self.preferences` to `preferences.json` on close."""
        if int(self.windowState()) == 0:
            self.preferences['geometry'] = self.geometry().getRect()
        self.preferences['window_state'] = int(self.windowState())
        self.preferences['wrap'] = self.wrap_btn.isChecked()
        self.preferences['font_size'] = self.txt.font().pointSize()
        with open('preferences.json', 'w') as file:
            json.dump(self.preferences, file)
            logger.info(f'To `preferences.json` following dictionary '
                        f'is saved: {self.preferences}')
        event.accept()

    @QtCore.pyqtSlot()
    def toggle_F11(self):
        """Toggles fullscreen mode."""
        if self.isFullScreen():
            self.showNormal()
        else:
            self.showFullScreen()

    @QtCore.pyqtSlot()
    def font_size_reset(self):
        """Resets font size to default."""
        self.set_font_size(DEFAULT_FONT_SIZE)

    @QtCore.pyqtSlot()
    def font_size_increase(self):
        """Increases fontsize by 1 on `Ctrl+Plus`."""
        current_font = self.txt.font()
        size = current_font.pointSize()
        if size < MAX_FONT_SIZE:
            self.set_font_size(size + 1)

    @QtCore.pyqtSlot()
    def font_size_decrease(self):
        """Decreases fontsize by 1 on `Ctrl+Minus`."""
        current_font = self.txt.font()
        size = current_font.pointSize()
        if size > MIN_FONT_SIZE:
            self.set_font_size(size - 1)

    @QtCore.pyqtSlot(str)
    def set_font_size(self, font_size):
        """Changes font size on input fields and sets font combobox to new fontsize."""
        current_font = self.txt.font()
        for widget in (self.pattern_line, self.flags_line, self.txt):
            widget.setFont(QtGui.QFont(current_font.family(), int(font_size)))

        self.font_size_cbbx.setCurrentText(str(int(font_size)))

    @QtCore.pyqtSlot()
    def wrap_switch(self, *args):
        """Toggle wrap mode."""
        if self.wrap_btn.isChecked():
            self.txt.setLineWrapMode(QtWidgets.QTextEdit.WidgetWidth)
        else:
            self.txt.setLineWrapMode(QtWidgets.QTextEdit.NoWrap)
        logger.debug(f'wrap_switch called. wrap_btn checked is {self.wrap_btn.isChecked()}')

    def set_color_rectangle(self):
        """Paints colored square on highlight color change button."""
        color_rectangle = QtGui.QPixmap(32, 32)
        color_rectangle.fill(QtCore.Qt.transparent)
        qp = QtGui.QPainter()
        qp.begin(color_rectangle)
        qp.setBrush(QtGui.QBrush(self.COLOR))
        qp.setPen(QtGui.QPen(QtCore.Qt.transparent))
        qp.drawRect(0, 0, 32, 32)
        qp.end()
        self.color_change_btn.setIcon(QtGui.QIcon(color_rectangle))
        logger.debug(
            f'Color of icon of color is changed on '
            f'{(self.COLOR.red(), self.COLOR.green(), self.COLOR.blue())}.'
        )

    @QtCore.pyqtSlot()
    def color_change_slot(self, color=None):  # color=None - for manual setting during testing
        """Slot to change highlight color."""
        if color is None:
            color = self.color_dialog.getColor()
        if color is None:
            return None
        if not color.isValid():
            return None
        self.COLOR = color
        self.set_color_rectangle()
        self.highlight()
        self.preferences['highlight_color'] = (self.COLOR.red(), self.COLOR.green(), self.COLOR.blue())
        with open('preferences.json', 'w') as file:
            json.dump(self.preferences, file)

    @QtCore.pyqtSlot()
    def get_regex(self):
        """Compiles regex only on pattern/flags change, not on text change."""
        pattern = self.pattern_line.text()
        flags = self.flags_line.text()
        try:
            self.regex = re.compile(pattern, flags=eval(flags) if flags else 0)
        except Exception as e:
            self.regex = re.compile('')
            logger_app_highlight.debug(f'Bad pattern: {pattern} or flag: {flags}; ' + repr(e))
        logger_app_highlight.debug(f'Input: pattern= {pattern}; flags= {flags};')

        self.highlight()

    @QtCore.pyqtSlot()
    def highlight(self):
        """Highlights in text according to set pattern."""
        txt = self.txt.toPlainText()
        logger_app_highlight.debug(f'Input: txt= {txt}')

        positions = list(self.regex.finditer(txt))

        # Select matches to highlight
        match_n = self.match_n_cbbx.currentText()
        mathch_n_string_list = [lang.get('all')] + [str(i + 1) for i in range(len(positions))]

        if match_n.isnumeric():
            if int(match_n) <= len(positions):
                positions = [positions[int(match_n) - 1]]

        # Prevent double self.highlight execution by disconnection-reconnection.
        self.match_n_cbbx.currentTextChanged.disconnect(self.highlight)
        if mathch_n_string_list != self.match_n_model.stringList():
            self.match_n_model.setStringList(mathch_n_string_list)
            self.match_n_cbbx.setCurrentText(match_n)
        self.match_n_cbbx.currentTextChanged.connect(self.highlight)

        gru2n = self.regex.groupindex
        n2gru = {gru2n[k]: k for k in gru2n}
        groups = ['0'] + [str(n2gru.get(g, g)) for g in range(1, self.regex.groups + 1)]
        logger_app_highlight.debug(f'Groups found: {groups}.')

        # Prevent double self.highlight execution by disconnection-reconnection.
        self.groups_cbbx.currentTextChanged.disconnect(self.highlight)
        if groups != self.groups_model.stringList():
            self.groups_model.setStringList(groups)
        self.groups_cbbx.currentTextChanged.connect(self.highlight)

        if not self.groups_cbbx.currentText():
            self.groups_cbbx.setCurrentText('0')

        if self.regex == re.compile(''):
            self.txt.tpls = []
        else:
            group = self.groups_cbbx.currentIndex()
            positions_tpls = [(p.span(group)[0], p.span(group)[1]) for p in positions]
            self.txt.tpls = positions_tpls
        self.txt.highlight(color=self.COLOR)


if __name__ == '__main__':

    app = App(sys.argv)
    logger.info('APPLICATION IS LOADED')
    sys.exit(app.exec())
