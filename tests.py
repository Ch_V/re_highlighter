from contextlib import contextmanager
import os
import re
import shutil
from time import sleep

from PyQt5 import QtCore, QtGui, QtWidgets
import pytest

from highlighter import Window, DEFAULT_FONT_SIZE
from widgets import TextEdit
from langs.en import lang


@pytest.mark.parametrize("inp", ('re.I', 're.S', 're.I | re.X', 're.A|re.M |re.S'))
def test_flags_line_input_good(qapp, inp):
    """Test of correct flags input."""
    qapp.window.flags_line.setText(inp)
    assert qapp.window.flags_line.hasAcceptableInput()


@pytest.mark.parametrize("inp", ('asdfs', 'KGHjkfd', 'os.system("")'))
def test_flags_line_input_wrong(qapp, inp):
    """Test of incorrect flags input."""
    qapp.window.flags_line.setText(inp)
    sleep(0.5)
    assert not qapp.window.flags_line.hasAcceptableInput()


def test_block_signals(qapp):
    """Context manager window.block_signals test."""
    res = 0

    def increase_res():
        nonlocal res
        res += 1
    qapp.window.testwidget = QtWidgets.QPushButton()
    qapp.window.testwidget.clicked.connect(increase_res)

    qapp.window.testwidget.click()
    assert res == 1

    with qapp.window.txt.block_signals(qapp.window.testwidget):
        qapp.window.testwidget.click()
    assert res == 1

    del qapp.window.testwidget


def test_txt_undo_redo_methods(qtbot):
    te = TextEdit(lang)
    te.show()
    qtbot.addWidget(te)
    te.setText('one')
    te.highlight()
    te.setText('two')
    te.highlight()
    te.undo()
    assert te.toPlainText() == 'one'
    te.undo()
    assert te.toPlainText() == ''
    te.undo()
    assert te.toPlainText() == ''
    te.redo()
    assert te.toPlainText() == 'one'
    te.redo()
    assert te.toPlainText() == 'two'
    te.redo()
    assert te.toPlainText() == 'two'


def test_txt_undo_redo_key(qtbot):
    te = TextEdit(lang)
    te.show()
    qtbot.addWidget(te)
    te.setText('one')
    te.highlight()
    te.setText('two')
    te.highlight()
    qtbot.keyPress(te, QtCore.Qt.Key_Z, modifier=QtCore.Qt.ControlModifier)
    assert te.toPlainText() == 'one'
    qtbot.keyPress(te, QtCore.Qt.Key_Z, modifier=QtCore.Qt.ControlModifier)
    assert te.toPlainText() == ''
    qtbot.keyPress(te, QtCore.Qt.Key_Z, modifier=QtCore.Qt.ControlModifier)
    assert te.toPlainText() == ''
    # 100663296 corresponds to 'Ctrl+Shift' modifiers.
    qtbot.keyPress(te, QtCore.Qt.Key_Z, modifier=QtCore.Qt.KeyboardModifiers(100663296))
    assert te.toPlainText() == 'one'
    qtbot.keyPress(te, QtCore.Qt.Key_Z, modifier=QtCore.Qt.KeyboardModifiers(100663296))
    assert te.toPlainText() == 'two'
    qtbot.keyPress(te, QtCore.Qt.Key_Z, modifier=QtCore.Qt.KeyboardModifiers(100663296))
    assert te.toPlainText() == 'two'


def test_remember_textcursor_pos(qapp):
    """Context manager window.remember_textcursor_pos test."""
    qapp.window.testwidget = QtWidgets.QTextEdit()
    qapp.window.testwidget.setText('0123456789')
    cursor = qapp.window.testwidget.textCursor()

    pos = cursor.position()
    with qapp.window.txt.remember_textcursor_pos(qapp.window.testwidget) as (cursor, pos):
        cursor.setPosition(7, 0)
        qapp.window.testwidget.setTextCursor(cursor)
    assert pos == cursor.position()

    del qapp.window.testwidget


@pytest.mark.parametrize('tpls, res', [
    ([(0, 3)], (1, 1, 1)),
    ([(2, 5), (11, 15)], (0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0)),
])
def test_txt_highlight(qapp, tpls, res):
    """Tests tuples highlighting in customized TextEditor."""
    qapp.window.COLOR = QtGui.QColor(0, 255, 0)
    qapp.window.txt.setText('a' * 20)
    qapp.window.txt.tpls = tpls
    qapp.window.txt.highlight()
    cursor = qapp.window.txt.textCursor()
    for tpl in tpls:
        for cp in range(tpl[0], tpl[1]):
            cursor.setPosition(cp, 0)
            cursor.setPosition(cp + 1, 1)
            color = cursor.charFormat().background().color().rgb()
            if color == 4278255360:
                char_is_highlighted = 1
            else:
                char_is_highlighted = 0
            assert char_is_highlighted == res[cp], f'cp = {cp}; {char_is_highlighted} =!= {res[cp]}'


def check_highlight(qapp, cursor, txt, res):
    """Function to be used in tests below.
    Checks highlighting of text txt by res mask (tuple of ones and zeros).
    """
    qapp.window.COLOR = QtGui.QColor(0, 255, 0)
    for cp, r in zip(range(0, len(txt)), res):
        cursor.setPosition(cp, 0)
        cursor.setPosition(cp + 1, 1)
        color = cursor.charFormat().background().color().rgb()
        if color == 4278255360:
            char_is_highlighted = 1
        else:
            char_is_highlighted = 0
        assert char_is_highlighted == r, f'cp = {cp}; {char_is_highlighted} =!= {r}'


@pytest.mark.parametrize("pattern, flags, txt, res", (
        [r'qwe', '', 'qwe', (1, 1, 1)],
        [r'qwe', '', 'Drqwey', (0, 0, 1, 1, 1, 0)],
        [r'qwe', 're.I', 'dfgqWemqwEng', (0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0)],
        [r'^.*?$', '', 'asjdgj\nasfsfg', (0,) * 13],
        [r'^.*?$', 're.S', 'asjdgj\nasfsfg', (1,) * 13],
        [r'^.*?$', 're.M', 'asjdgj\nasfsfg', (1,) * 13],
        [r'\A.*?\Z', '', 'asjdgj\nasfsfg', (0,) * 13],
        [r'\A.*?\Z', 're.M', 'asjdgj\nasfsfg', (0,) * 13],
        [r'\A.*?\Z', 're.S', 'asjdgj\nasfsfg', (1,) * 13],
        [r'^(.(?<!qwe))*$', 're.I | re.M', 'qWekjhdsg\nfasdfUh\nasdqWEsdg\njfhdqwe', (0,) * 9 + (1,) * 8 + (0,) * 18],
))
def test_highlighting(qapp, pattern, flags, txt, res):
    """Tests pattern highlighting."""
    qapp.window.pattern_line.setText(pattern)
    qapp.window.flags_line.setText(flags)
    qapp.window.txt.setText(txt)
    cursor = qapp.window.txt.textCursor()
    check_highlight(qapp, cursor, txt, res)


def test_text_change_with_match_setted(qapp):
    """Tests application behaviour when text is being changed with match set not to 'All'."""
    qapp.window.pattern_line.setText('qwe')
    qapp.window.txt.setText('qweqweqwe')
    cursor = qapp.window.txt.textCursor()

    qapp.window.match_n_cbbx.setCurrentText('3')
    check_highlight(qapp, cursor, qapp.window.txt.toPlainText(), (0,0,0, 0,0,0, 1,1,1))

    qapp.window.txt.setText('qweqweqw')
    check_highlight(qapp, cursor, qapp.window.txt.toPlainText(), (1,1,1, 1,1,1, 0,0))
    assert qapp.window.match_n_cbbx.currentText() == 'All'

    qapp.window.txt.setText('qweqweqwe')
    qapp.window.match_n_cbbx.setCurrentText('2')
    check_highlight(qapp, cursor, qapp.window.txt.toPlainText(), (0,0,0, 1,1,1, 0,0,0))

    qapp.window.txt.setText('qweqwqwe')
    qapp.processEvents()
    check_highlight(qapp, cursor, qapp.window.txt.toPlainText(), (0,0,0, 0,0, 1,1,1))
    assert qapp.window.match_n_cbbx.currentText() == '2'


@pytest.mark.parametrize("pattern, grp, match, flags, txt, res", (
    [r'qwe', '0', 'all', '', 'qwe', (1, 1, 1)],
    [r'(qwe)', '1', 'all', '', 'qwe', (1, 1, 1)],
    [r'(q)A(d)tgh', '2', 'all', '', 'qAdtgh', (0, 0, 1, 0, 0, 0)],
    [r'k(jsd)kh(?P<mygr>ks)jhf(kjh)', 'mygr', 'all',  '', 'kjsdkhksjhfkjh', (0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0)],
    [r'k(jsd)kh(?P<mygr>ks)jhf(kjh)', '3', 'all', '', 'kjsdkhksjhfkjh', (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1)],
    [r'(q)w(e)', '0', 'all', 'qweqweqwe', '', (1, 1, 1, 1, 1, 1, 1, 1, 1)],
    [r'(q)w(e)', '2', '2', 'qweqweqwe', '', (0, 0, 0, 0, 0, 1, 0, 0, 0)],
    [r'(q)w(e)', '1', '1', 'qweqweqwe', '', (1, 0, 0, 0, 0, 0, 0, 0, 0)],
))
def test_highlighting_group_and_match(qapp, pattern, grp, match, flags, txt, res):
    """Test groups highlighting."""
    qapp.window.pattern_line.setText(pattern)
    qapp.window.groups_cbbx.setCurrentText(grp)
    qapp.window.match_n_cbbx.setCurrentText(match)
    qapp.window.flags_line.setText(flags)
    qapp.window.txt.setText(txt)
    cursor = qapp.window.txt.textCursor()
    check_highlight(qapp, cursor, txt, res)


def test_f11_btn(qapp):
    """Test FullSceen button"""
    qapp.window.showNormal()
    qapp.window.f11_btn.click()
    assert qapp.window.isFullScreen(), 'didn`t change to fullscreen.'
    qapp.window.f11_btn.click()
    assert not qapp.window.isFullScreen(), 'didn`t change from fullscreen.'
    qapp.window.showNormal()


def test_f11_key(qapp):
    """Test F11 key for FullSceen"""
    qapp.window.showNormal()
    qapp.window.keyPressEvent(QtGui.QKeyEvent(QtCore.QEvent.KeyPress, QtCore.Qt.Key_F11, QtCore.Qt.NoModifier))
    assert qapp.window.isFullScreen(), 'didn`t change to fullscreen.'
    qapp.window.keyPressEvent(QtGui.QKeyEvent(QtCore.QEvent.KeyPress, QtCore.Qt.Key_F11, QtCore.Qt.NoModifier))
    assert not qapp.window.isFullScreen(), 'didn`t change from fullscreen.'
    qapp.window.showNormal()


def test_wrap_btn(qapp):
    """Test wrap"""
    qapp.window.wrap_btn.setChecked(False)
    assert qapp.window.txt.lineWrapMode() == QtWidgets.QTextEdit.NoWrap
    qapp.window.wrap_btn.toggle()
    assert qapp.window.txt.lineWrapMode() == QtWidgets.QTextEdit.WidgetWidth
    assert qapp.window.wrap_btn.isChecked(), 'wrap_btn didn`t stuck!'
    qapp.window.wrap_btn.toggle()
    assert qapp.window.txt.lineWrapMode() == QtWidgets.QTextEdit.NoWrap
    assert not qapp.window.wrap_btn.isChecked(), 'wrap_btn didn`t unstuck!'


class TestOpenClose:
    """CLass of test with main window open-close actions."""
    def test_app_reset_for_futher_tests(self, qapp):
        """Resets """
        qapp.window.groups_cbbx.setCurrentText('0')
        qapp.window.match_n_cbbx.setCurrentText('all')

    def test_highlight_color_change_and_load(self, qtbot):
        """Test for color change and color load from `preferences.json`."""
        window = Window()
        window.show()
        qtbot.addWidget(window)

        def get_color():
            window.pattern_line.setText('qwe')
            window.flags_line.setText('')
            window.txt.setText('qwe')
            cursor = window.txt.textCursor()
            clr = cursor.charFormat().background().color().toRgb()
            clr = (clr.red(), clr.green(), clr.blue())
            return clr

        color = get_color()
        window.color_change_slot(color=QtGui.QColor(*[(channel + 100) % 255 for channel in color]))
        color_new = get_color()
        window = Window()
        window.show()
        color_loaded = get_color()
        assert all([c != cn for c, cn in zip(color, color_new)]), \
            (color, '=', color_new, 'color didn`t change!')
        assert all([cl == cn for cl, cn in zip(color_loaded, color_new)]), \
            (color_loaded, '=!=', color_new, 'color isn`t loaded!')

    def test_geometry_recall(self, qapp):
        """Test geometry restore on window close-open."""
        geometry = qapp.window.geometry().getRect()
        new_geometry = [((i + 100) % 222 + 180) for i in geometry]  # should not set size less than minimum!
        assert new_geometry != geometry, 'geometry isn`t changed.'
        qapp.window.setGeometry(*new_geometry)

        qapp.window.close()
        qapp.window.deleteLater()
        qapp.window = Window()
        qapp.window.show()

        assert qapp.window.geometry() == QtCore.QRect(*new_geometry), 'geometry isn`t restored.'

    def test_wrap_recall(self, qapp):
        """Test `Wrap` button on open-close."""
        qapp.window.wrap_btn.setChecked(True)

        qapp.window.close()
        qapp.window.deleteLater()
        qapp.window = Window()
        qapp.window.show()

        assert qapp.window.txt.lineWrapMode() == QtWidgets.QTextEdit.WidgetWidth

        qapp.window.wrap_btn.setChecked(False)

        qapp.window.close()
        qapp.window.deleteLater()
        qapp.window = Window()
        qapp.window.show()

        assert qapp.window.txt.lineWrapMode() == QtWidgets.QTextEdit.NoWrap

    def test_fontsize_recall(self, qapp):
        """Test font size restore on close-open
        and keyboard font size reset-increase-decrease (`Ctrl+0`-`Ctrl+Plus`-`Ctrl+Minus`).
        """
        old_fontsize = qapp.window.txt.font().pointSize()
        qapp.window.set_font_size((old_fontsize + 5) % 50 + 5)
        fontsize = qapp.window.txt.font().pointSize()

        assert fontsize != old_fontsize, 'Font size isn`t changed.'

        qapp.window.close()
        qapp.window.deleteLater()
        qapp.window = Window()
        qapp.window.show()

        new_fontsize = qapp.window.txt.font().pointSize()
        assert fontsize == new_fontsize, 'Font size isn`t restored.'

        qapp.window.set_font_size(55)
        qapp.window._font_size_reset.activated.emit()

        assert qapp.window.txt.font().pointSize() == DEFAULT_FONT_SIZE, 'Font size didn`t reset on Ctrl+0.'

        qapp.window._font_size_increase.activated.emit()
        assert qapp.window.txt.font().pointSize() == DEFAULT_FONT_SIZE + 1, 'Font size didn`t increase on Ctrl+Plus.'

        # Two times to get under DEFAULT_FONT_SIZE.
        qapp.window._font_size_decrease.activated.emit()
        qapp.window._font_size_decrease.activated.emit()
        assert qapp.window.txt.font().pointSize() == DEFAULT_FONT_SIZE - 1, 'Font size didn`t decrease on Ctrl+Minus.'


def test_lang_change(qapp):
    """Language change test. Includinf TextEdit and ColorDialog."""
    assert 'En' in qapp.langs_available and 'Ru' in qapp.langs_available, \
        'You need "en.py" and "ru.py" language files to run this test.'

    qapp.window.lang_cbbx.setCurrentText('En')
    assert re.fullmatch(r'.*[a-z ]+', qapp.window.font_size_lbl.text(), re.I)
    assert not re.fullmatch(r'.*[а-яё ]+.*', qapp.window.font_size_lbl.text(), re.I)

    assert re.fullmatch(r'[a-z&\t+ ]+', qapp.window.txt.menu.actions()[0].text(), re.I)
    assert not re.fullmatch(r'[а-яё ]+.*', qapp.window.txt.menu.actions()[0].text(), re.I)

    assert re.fullmatch(r'[a-z& ]+', qapp.window.color_dialog.widget.children()[2].text(), re.I)
    assert not re.fullmatch(r'[а-яё ]+.*', qapp.window.txt.menu.actions()[0].text(), re.I)

    qapp.window.lang_cbbx.setCurrentText('Ru')

    assert not re.fullmatch(r'.*[a-z ]+', qapp.window.font_size_lbl.text(), re.I)
    assert re.fullmatch(r'.*[а-яё ]+.*', qapp.window.font_size_lbl.text(), re.I)

    assert not re.fullmatch(r'[a-z\t+ ]+', qapp.window.txt.menu.actions()[0].text(), re.I)
    assert re.fullmatch(r'[а-яё ]+.*', qapp.window.txt.menu.actions()[0].text(), re.I)

    assert not re.fullmatch(r'[a-z& ]+', qapp.window.color_dialog.widget.children()[2].text(), re.I)
    assert re.fullmatch(r'[а-яё ]+.*', qapp.window.txt.menu.actions()[0].text(), re.I)


@pytest.mark.parametrize('del1, del2, lang_cbbx_res', (
        ['ru', 'en', ''],
        ['en', '', 'Ru'],
        ['ru', '', 'En'],
))
def test_lang_load_no_langs(qapp, del1, del2, lang_cbbx_res):
    """Test application workability when some language files are missing.
    Doesn`t cover all possible cases.
    """
    assert os.path.isfile('langs/en.py') and os.path.isfile('langs/ru.py'), \
        'You need "en.py" and "ru.py" language files to run this test.'

    @contextmanager
    def closed_app(app):
        """Allows to do some actions in between application close-open"""
        app.application_start()
        app.window.close()
        app.window.deleteLater()
        yield
        app.application_start()
        app.window = Window()
        app.window.show()

    def recopy_langs():
        """Restores language files in tmpdir if they are missing."""
        base = os.path.dirname(os.path.abspath(__file__))
        for filename in ('en.py', 'ru.py'):
            try:
                shutil.copy(os.path.join(base, 'langs', filename), os.path.join(os.getcwd(), 'langs', filename))
            except FileExistsError:
                pass

    with closed_app(qapp):
        if del1:
            os.remove(f'langs/{del1}.py')
        if del2:
            os.remove(f'langs/{del2}.py')

    # Get language files back again.
    recopy_langs()

    assert qapp.window.lang_cbbx.currentText() == lang_cbbx_res
