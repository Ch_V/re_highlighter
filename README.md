Русский вариант README можно найти в файле `README_RU.md`.
# Regular Expression Highlighter.

Simple but fully functional GUI application to highlight python regular expressions.

![RE_highlighter](https://gitlab.com/Ch_V/re_highlighter/uploads/6399feb5b87ba88377fb12c6d429d2ff/REHL-EN.png)

### System Requirements.

* <ins>*Windows 7+*</ins> or <ins>*GNU/Linux*</ins>. Probably some other OSs are compatible too but it wasn't tested.

* <ins>*Python3*</ins> and <ins>*PyQt5*</ins>.

* If you want to run tests you also need <ins>*pytest*</ins> and <ins>*pytest-qt*</ins>.

### Insatallation.

#### Windows:
* Insatall [Python3](https://www.python.org/downloads/).
  
  _(*.pyw files should be associated with `C:\Program Files\Python<YourPythonVersion>\pythonw.exe`. 
  Usually it is done automatically.)_
* Install [PyQt5](https://www.riverbankcomputing.com/software/pyqt/) 
  with command `pip install pyqt5` in command line.
* Save files of the program to any place (read-write access is required) you want.
* For your convenience create a desktop link to `__main__.pyw` file 
  and set the link icon to `icon.ico`.
* ENJOY / PROFIT

#### GNU/Linux:
(example for Debian-based distributive)

* Install [Python3](https://www.python.org/downloads/) (if it is not installed yet)
  with command `apt update && apt install python3` in terminal.
* Install [PyQt5](https://www.riverbankcomputing.com/software/pyqt/) (if it is not installed yet)
  with command `apt update && apt install python3-pyqt5` in terminal.
* Save files of the program to any place (read-write access is required) you want.
* Rename `__main__.pyw` to `__main__.py` (or just register `*.pyw` filetype as python script).
* Open in text editor `__main__.py` file and check if its first line `#!/usr/bin/python3` 
  correctly points to your python3 interpreter location (`which python3` in terminal) 
  and correct it if it is necessary.
* Make `__main__.py` file executable (`chmod a+x __main__.py` in terminal). 
  Probably you also need to change your file manager default behaviour to run rather than open executable files.
* For your convenience create a `REHL.desktop` (you can name it as you want) Desktop Entity application link, 
  configure it to run `__main__.py`, 
  make it executable (`chmod a+x REHL.desktop` in terminal) 
  and set the link icon to `icon.ico`.
  
  Simple example of corresponding `REHL.desktop` file is appended 
  (You can customize it. Don't forget to <ins>change paths to yours</ins> in it if you decide to use appended one).
  
  For application registration you may place a copy of this Desktop Entity file
  to `/usr/share/applications` or `~/.local/share/applications/`.
  
  _For GNOME users: to make Desktop Entity (*.desktop) runnable from desktop you should do following extra steps:_
  * _Place it on your desktop._
  * _Make it Read-only (not Read and Write!) for Others access._
  * _Click on appeared after that `Allow Launching` Desktop Entity context menu line._
  

* ENJOY / PROFIT
