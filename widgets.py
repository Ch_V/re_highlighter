"""Some subclassed widgets."""

from contextlib import contextmanager

from PyQt5 import QtCore, QtGui, QtWidgets

from constants_and_loggers import TXT_UNDO_LEN, logger, logger_txt_history, logger_txt_highlight


class TextEdit(QtWidgets.QTextEdit):
    """Subclassed QTextEdit.
    QSyntaxHighlighter can highlight only inside text block. But here we want
    to be able to highlight despite of text blocks boundaries (including "\n").
    So we have to create our own method. And so, we have to redefine undo-redo
    functionality too. New undo step records on self.highlight() method call.
    """
    def __init__(self, lang, parent=None):
        super().__init__(parent=parent)
        self.lang = lang
        self.history = [("", 0)]  # full history of states for undo
        self.history_ = self.history  # history of states for undo til current state
        self.setAcceptRichText(False)

        # UNDO
        self.undo_action = QtWidgets.QAction(self.lang.get('undo', ''))
        self.undo_action.triggered.connect(self.undo)

        self.menu = self.createStandardContextMenu()
        self.undo_action.setIcon(self.menu.actions()[0].icon())
        self.menu.removeAction(self.menu.actions()[0])
        self.menu.insertAction(self.menu.actions()[0], self.undo_action)

        # REDO
        self.redo_action = QtWidgets.QAction(self.lang.get('redo', ''))
        self.redo_action.triggered.connect(self.redo)

        self.redo_action.setIcon(self.menu.actions()[1].icon())
        self.menu.removeAction(self.menu.actions()[1])
        self.menu.insertAction(self.menu.actions()[1], self.redo_action)

        self.menu.actions()[3].setText(self.lang.get('cut', ''))
        self.menu.actions()[4].setText(self.lang.get('copy', ''))
        self.menu.actions()[5].setText(self.lang.get('paste', ''))
        self.menu.actions()[6].setText(self.lang.get('delete', ''))
        self.menu.actions()[8].setText(self.lang.get('select all', ''))

        # Enable/Disable context menu undo/redo items on menu creation.
        if len(self.history_) <= 1:
            self.menu.actions()[0].setEnabled(False)
        else:
            self.menu.actions()[0].setEnabled(True)
        if len(self.history_) < len(self.history):
            self.menu.actions()[1].setEnabled(True)
        else:
            self.menu.actions()[1].setEnabled(False)

        # Set text formatters.
        self.fmt_h = QtGui.QTextCharFormat()  # highlight format
        self.fmt_c = QtGui.QTextCharFormat()  # clear format
        self.tpls = []  # list of tuples `[(start, end), (start, end), ...]` positions to highlight

        self.installEventFilter(self)

    def eventFilter(self, obj: QtCore.QObject, event: QtCore.QEvent) -> bool:
        """Filters Ctrl+MouseWheel from scrolling effect (but keeps font size change effect)."""
        if isinstance(event, QtGui.QWheelEvent):
            if event.modifiers() & QtCore.Qt.ControlModifier:
                return True
        return False

    def contextMenuEvent(self, event: QtGui.QContextMenuEvent) -> None:
        """Show context menu."""
        self.menu.move(self.mapToGlobal(event.pos()))
        self.menu.show()

    def keyPressEvent(self, event):
        """Key press events capturing to redefine Ctrl+Z and Ctrl+Shift+Z behavior."""
        cursor = self.textCursor()
        if event.key() == QtCore.Qt.Key_Z \
                and event.modifiers() == QtCore.Qt.ControlModifier:
            with self.block_signals(widget=self):
                if self.undo():
                    # To get the remaining keypress functionality back (typing etc).
                    QtWidgets.QTextEdit.keyPressEvent(self, event)
                    cursor.setPosition(self.history_[-1][1], 0)
                    self.setTextCursor(cursor)
        elif event.key() == QtCore.Qt.Key_Z \
                and event.modifiers() == (QtCore.Qt.ControlModifier | QtCore.Qt.ShiftModifier):
            with self.block_signals(widget=self):
                self.redo()
                # To get the remaining keypress functionality back (typing etc).
                QtWidgets.QTextEdit.keyPressEvent(self, event)
                cursor.setPosition(self.history_[-1][1], 0)
                self.setTextCursor(cursor)
        else:
            # To get the remaining keypress functionality back (typing etc).
            QtWidgets.QTextEdit.keyPressEvent(self, event)

    def undo(self):
        logger.debug('TextEdit.undo method is called.')
        cursor = self.textCursor()
        if len(self.history_) <= 1:
            return None
        self.history_ = self.history_[:-1]
        # Reset text.
        self.setText(self.history_[-1][0])
        self.tpls = []
        self.highlight()
        self.setTextCursor(cursor)
        return True if len(self.history_) > 0 else False

    def redo(self):
        logger.debug('TextEdit.redo method is called.')
        cursor = self.textCursor()
        if len(self.history_) >= len(self.history):
            return None
        self.history_ = self.history[:len(self.history_) + 1]
        # Reset text.
        self.setText(self.history_[-1][0])
        self.highlight()
        self.setTextCursor(cursor)

    @QtCore.pyqtSlot()
    def remember(self):
        """Remembers undo-history step."""
        cursor = self.textCursor()
        # Remember text and textcursor position if text changed to undo history
        if self.history_[-1][0] != self.toPlainText():
            self.history_ = self.history_[-TXT_UNDO_LEN:]
            self.history = self.history_
            self.history.append((self.toPlainText(), cursor.position()))
        # Enable/Disable context menu items on text change.
        if len(self.history_) <= 1:
            self.menu.actions()[0].setEnabled(False)
        else:
            self.menu.actions()[0].setEnabled(True)
        if len(self.history_) < len(self.history):
            self.menu.actions()[1].setEnabled(True)
        else:
            self.menu.actions()[1].setEnabled(False)

    @QtCore.pyqtSlot()
    def highlight(self, color=QtGui.QColor(0, 255, 0)):
        """Highlights text according to self.tpls positions with
        self.fmt_h (for highlighted area) and self.fmt_c (for not highlighted area) format.
        """
        if self.tpls:
            if max((max(tpl) for tpl in self.tpls)) > len(self.toPlainText())\
                    or min((min(tpl) for tpl in self.tpls)) < 0:
                logger_txt_highlight.warning('tpls highlight boundaries out of text range!!!'
                                             f'tpls: {self.tpls}, text: {self.toPlainText()}')

        self.fmt_h.setBackground(color)
        logger_txt_history.debug(f'Undo history is: {self.history}')
        logger_txt_history.debug(f'Undo history_ is: {self.history_}')
        with self.block_signals(self):
            with self.remember_textcursor_pos() as (cursor, pos):
                # Remember text and textcursor position if text changed to undo history
                self.remember()

                # Select and format.
                self.highlightl_clear()
                logger_txt_highlight.debug(f'Text highlighting formatting is cleared.')
                for tpl in self.tpls:
                    logger_txt_highlight.debug(f'Fragment to highlight: {tpl}')
                    cursor.setPosition(tpl[0], 0)
                    cursor.setPosition(tpl[1], 1)
                    cursor.setCharFormat(self.fmt_h)
                if self.tpls:
                    cursor.setPosition(tpl[1], 0)  # deselect

    def highlightl_clear(self, clear_format=None):
        """Clears highlighting formatting in whole self."""
        if clear_format is None:
            clear_format = self.fmt_c
        txt = self.toPlainText()
        cursor = self.textCursor()
        cursor.setPosition(0, 0)
        cursor.setPosition(len(txt), 1)
        cursor.setCharFormat(clear_format)

    @contextmanager
    def remember_textcursor_pos(self, widget=None):
        """Remembers and resets text cursor and scrollbars positions."""
        if widget is None:
            widget = self
        cursor = widget.textCursor()
        pos = cursor.position()
        h_pos = widget.horizontalScrollBar().value()
        v_pos = widget.verticalScrollBar().value()

        yield cursor, pos

        cursor.setPosition(pos, 0)
        widget.setTextCursor(cursor)
        widget.horizontalScrollBar().setValue(h_pos)
        widget.verticalScrollBar().setValue(v_pos)

    @contextmanager
    def block_signals(self, widget=None):
        """Blocks and releases self.txt signals receiving to prevent recursion."""
        if widget is None:
            widget = self.txt
        widget.blockSignals(True)
        yield
        widget.blockSignals(False)


class ColorDialog(QtWidgets.QDialog):
    """Subclassed QDialog for QColorDialog customization (internationalization) purpose."""
    def __init__(self, lang, parent=None):
        super().__init__(parent, windowTitle=lang.get('ColorDialog_title', ''))
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.widget = QtWidgets.QColorDialog(self)
        self.widget.setWindowFlags(QtCore.Qt.Widget)
        self.widget.setOptions(
            QtWidgets.QColorDialog.DontUseNativeDialog |
            QtWidgets.QColorDialog.NoButtons
        )

        # Add Ok-Cancel buttons
        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.widget)
        hbox = QtWidgets.QHBoxLayout()
        hbox.setDirection(QtWidgets.QHBoxLayout.RightToLeft)
        sp = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        self.cancel_btn = QtWidgets.QPushButton(lang.get('ColorDialog_cancel_btn', 'Cancel'))
        self.ok_btn = QtWidgets.QPushButton(lang.get('ColorDialog_ok_btn', 'Ok'))
        self.cancel_btn.setSizePolicy(sp)
        self.ok_btn.setSizePolicy(sp)
        hbox.addWidget(self.cancel_btn, stretch=0, alignment=QtCore.Qt.AlignRight)
        hbox.addWidget(self.ok_btn, stretch=0, alignment=QtCore.Qt.AlignRight)
        hbox.addStretch(1)
        layout.addLayout(hbox)

        self.cancel_btn.clicked.connect(self.close)
        self.ok_btn.clicked.connect(self._get_color)

        # Internationalization.
        for i, ch in enumerate(self.widget.children()):
            if i in (2, 3, 6, 7):
                ch.setText(lang.get('ColorDialog_' + str(i), ''))
            if i == 10:
                for i2, ch2 in enumerate(ch.children()):
                    if isinstance(ch2, QtWidgets.QLabel):
                        ch2.setText(lang.get('ColorDialog_c_' + str(i2), ''))

        self.color = None
        self.widget.installEventFilter(self)

    def eventFilter(self, obj: QtCore.QObject, event: QtCore.QEvent) -> bool:
        """Filters `ESC` keypress for self.widget to prevent its destroy.
        (`ESC` should close only self window).
        """
        if event.type() == QtCore.QEvent.KeyPress:
            if event.key() == QtCore.Qt.Key_Escape:
                obj.parent().close()
                return True
        return False

    def getColor(self):
        """The same as QtWidgets.QDialog.getColor() but for customized ColorDialog."""
        self.exec()
        return self.color

    @QtCore.pyqtSlot()
    def _get_color(self):
        """Finalizes ColorDialog with Ok button.
        Sets self.color for self.getColor method to return.
        """
        self.color = self.widget.currentColor()
        self.close()


if __name__ == '__main__':
    from langs.en import lang
    app = QtWidgets.QApplication([])
    window = TextEdit(lang=lang)

    ## TEXT EDIT
    # window.tpls = [(0, 3), (5, 10), (12, 15)]
    # window.setText('Hello world!\nworld, world, world...')
    # window.highlight()

    ## COLOR DIALOG
    # cd = ColorDialog(lang=lang)
    # res_color = cd.getColor()
    # print(res_color.toRgb().red(), res_color.toRgb().green(), res_color.toRgb().blue())

    window.show()
    app.exec()
