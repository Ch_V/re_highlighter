import os
import shutil

# from PyQt5 import QtCore, QtWidgets
import pytest

from highlighter import App


@pytest.fixture(scope='session', autouse=True)
def copy_data(tmp_path_factory):
    """Change working directory to tmp (to prevent changes in original `preferences.json`).
    Copies files application is needed to tmp destination.
    """
    os.chdir(tmp_path_factory.getbasetemp())
    base = os.path.dirname(os.path.abspath(__file__))
    # Copy files.
    for filename in ('icon.ico', 'wrap_icon.svg', 'F11_icon.svg'):
        shutil.copy(os.path.join(base, filename), os.path.join(os.getcwd(), filename))
    shutil.copytree(os.path.join(base, 'langs'), os.path.join(os.getcwd(), 'langs'))


# @pytest.fixture(scope='session')  # scope='function' is possible to but much slower.
# def app_c(tmp_path_factory):
#     """Fixture to create, open and finally close application. Deprecated due to pytest-qt."""
#     app = App([])
#     timer = QtCore.QTimer()
#     timer.singleShot(50, QtWidgets.QApplication.instance().quit)
#     app.exec()
#     yield app
@pytest.fixture(scope="session")
def qapp():
    """pytest-qt application instance"""
    yield App([])