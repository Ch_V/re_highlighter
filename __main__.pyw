#!/usr/bin/python3

# RE_highlighter (Regular Expression Highlighter)
# Copyright (C) 2021  Chelpanov Vitaly (https://gitlab.com/Ch_V/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import os
import sys

from highlighter import App
from constants_and_loggers import logger

# Paths correction when launched from any directory.
os.chdir(os.path.dirname(os.path.abspath(__file__)))

app = App(sys.argv)
logger.info('APPLICATION IS LOADED')
sys.exit(app.exec())
